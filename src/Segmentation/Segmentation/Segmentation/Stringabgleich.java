package Segmentation;
import java.util.*;

public class Stringabgleich {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// given sample Strings
		String lexicon = "ada 23 abb 354 abc 1";
		String tags = "gfb/NN abc/VBN hxy/Det adc/VB ./. gfb/NN abc/VBN hxy/Det ada/VB ./. gfb/NN abc/VBN hxy/Det ada/VB ./.";
		char c = 46;
		String eos = ".";
		//eos=String.valueOf(c);
		
		//transfer Strings in lists
		String[] lexEntry = lexicon.split("\\d{1,3}");
		String[] tagEntry = tags.split("\\s");

		//remove white spaces before and after
		for(int x=0; x<lexEntry.length; x++) {
			lexEntry[x]=lexEntry[x].trim();
			//System.out.println(lexEntry[x]);
		}
		for(int y=0; y<tagEntry.length; y++) {
			tagEntry[y]=tagEntry[y].trim();
			//System.out.println(tagEntry[y]);
		}

		//match lexicon with tags
		String[] tagPattern = new String[tagEntry.length];
                for (int x=0; x<lexEntry.length; x++){
                    for( int y=0; y<tagEntry.length; y++){
                        if (lexEntry[x].regionMatches(0,tagEntry[y],0,lexEntry[x].length())){
                            if(tagPattern[y] == null)
                            	tagPattern[y]=tagEntry[y];
                        } // end of first if
                    } // end of second for-loop tagEntry
                } //end of first for-loop lexEntry

         //insert periods, question & exclamation marks in pattern String
                for (int x=0; x<tagEntry.length; x++) {
                	if (tagEntry[x].substring(tagEntry[x].indexOf("/")+1).equals(eos)){
                		tagPattern[x] =  tagEntry[x];
                	}
         //mark null-slots with xxx
                	else if (tagPattern[x] == null)
                		tagPattern[x] = "xxx";
                }

         //output word with tags
                for(int x=0; x<tagPattern.length; x++) {
                        tagPattern[x]=tagPattern[x].trim();
                        System.out.print(tagPattern[x] + " ");
                }
                System.out.println();

         //remove words from tag pattern
                for(int x=0; x<tagPattern.length; x++) {
                	if (!tagPattern[x].equals("xxx")){
                		tagPattern[x] = tagPattern[x].substring(tagPattern[x].indexOf("/")+1);
                	}
                System.out.print(tagPattern[x] + " ");
                }
                System.out.println();

         //output pattern without missed words
                for(int x=0; x<tagPattern.length; x++){
                	if (!tagPattern[x].equals("xxx")) System.out.print(tagPattern[x] + " ");
                }
                System.out.println();

         //output tag pattern on sentence level
                String sentence= arrayToString(tagPattern, " ");
                String[] sentencePattern = sentence.split(eos);
                
                for (int x=0; x<sentencePattern.length; x++) {
                	sentencePattern[x] = sentencePattern[x].trim();
                	int y = x + 1;
                	System.out.println("Sentence Pattern " + y + ": "  + sentencePattern[x]);
                }
                
         //output words including placeholders       
                freqDist(sentencePattern);
                System.out.println();
                
         // delete placeholders xxx
                for(int z=0; z<tagPattern.length; z++){
                	if(tagPattern[z].equals("xxx")){
                		tagPattern[z] = tagPattern[z].replace(tagPattern[z], "");                		
                	}                	
                }
                String noPlaceholderSentence = arrayToString(tagPattern, " ");
                noPlaceholderSentence = noPlaceholderSentence.replaceAll("  ", " ");
                String[] noPlaceholderPattern = noPlaceholderSentence.split("eos");
                for(int x=0; x<noPlaceholderPattern.length; x++){
                	//delete whitespaces before and after respective unit
                	noPlaceholderPattern[x] = noPlaceholderPattern[x].trim();
                }
               
          //output most frequent pattern without words missed       
                freqDist(noPlaceholderPattern);
                System.out.println();
                
	}// main method end

	//method sorts and counts occurrences
	
	private static void freqDist(String[] sentencePattern){
		if (sentencePattern.length>1){//checks if at least two entries can be compared
			//sort Array 
			Arrays.sort(sentencePattern);
			//counting procedure start
			for(int x=0; x<sentencePattern.length; x++){	
				System.out.println(sentencePattern[x]);
			}
            for(int x=0; x<sentencePattern.length; x++){                	
            	int i = 1;
                for(int y=x+1; y<sentencePattern.length; y++){                		
                	if (sentencePattern[x].equals(sentencePattern[y]) && !sentencePattern[x].equals("")) {
                		// to avoid double counts, counted elements are deleted
                		sentencePattern[y] = sentencePattern[y].replace(sentencePattern[y], "");
                		i++; // number of occurrences            	
                	}
                }
            	if (i>1) System.out.println("Ergebnis Vergleich in Runde " + x + ": " + sentencePattern[x] + " " + i );	
            } //end of for-loop
        } //end of first if
        else System.out.println("There is only one entry in the File!");
	}
	
	// method to transverse a Stringarray to a String
	private static String arrayToString(String[] a, String separator) {
	    String result = "";
	    if (a.length > 0) {
	        result = a[0];    // start with the first element
	        for (int i=1; i<a.length; i++) {
	            result = result + separator + a[i];
	        }
	    }
	    return result;
	}

}// end of class

