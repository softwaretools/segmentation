/*
 *
 * Usage:
 *    java TabbedChains <inFile> <outFile> [ipa]
 *
 *
 */

package Segmentation;
import java.util.*;
import java.io.*;


public class TabbedChains
{
  static boolean ipa = false;

  public static void main(String[] args)
  {
    for (int i = 1; i < args.length; i++)
    {
      switch(i)
      {
        case 2: if("ipa".equals(args[i])) ipa = true; break;
      }
    }
    System.out.println(ipa);
    String dataOut = convertToTabbed(args[0],args[1]);
    IPA.writeFile(args[1],dataOut,false);
  }

  static String convertToTabbed(String fileIn, String fileOld)
  {
    String dataIn = ipa ? IPA.convertIPAToASCII(IPA.readFile(fileIn,true)) : IPA.readFile(fileIn,false);
    String dataOld = ((new File(fileOld)).exists()) ? IPA.readFile(fileOld,false) : null;

    Hashtable chainsNewHash = getNewChains(dataIn);

    int firstReturn = dataOld!=null ? dataOld.indexOf("\n") : -1;
    String[] chainsOld = firstReturn>=0 ? getOldChains(dataOld.substring(0,firstReturn)) : new String[0];

    Vector chainsAllVector = new Vector(Arrays.asList(chainsOld));
    Set set= chainsNewHash.keySet();
    Iterator iter = set.iterator();
    while(iter.hasNext())
    {
      String s = (String) iter.next();
      if(!chainsAllVector.contains(s)) chainsAllVector.addElement(s);
    }

    chainsAllVector.remove("");

    String dataOutFirstLine = String.valueOf((char) 9)+String.valueOf((char) 9);
    String dataOutNewTable = fileIn + String.valueOf((char) 9);
    for(int i=0;i<chainsAllVector.size();i++)
    {
      dataOutFirstLine += ((String) chainsAllVector.elementAt(i));
      dataOutNewTable += chainsNewHash.containsKey(((String) chainsAllVector.elementAt(i))) ? chainsNewHash.get(((String) chainsAllVector.elementAt(i))) : "";
      dataOutNewTable += String.valueOf((char) 9);
      if(i<chainsAllVector.size()-1) dataOutFirstLine += String.valueOf((char) 9);
    }

    String dataOutOldTable = firstReturn>=0 ? dataOld.substring(firstReturn+1) : "";

    String dataOut = dataOutFirstLine + "\n" + dataOutOldTable + dataOutNewTable +"\n";
    return(dataOut);
  }

  static Hashtable getNewChains(String dataIn)
  {
    String[] chainsArray = dataIn.split("\\s+");
    Hashtable newChains = new Hashtable();
    for(int i=0;i<chainsArray.length;i+=2)
    {
      String newChain = chainsArray[i];
      newChains.put(newChain,(chainsArray[i+1]));
      System.out.print(chainsArray[i]+" # ");System.out.println((chainsArray[i+1]));
    }
    return(newChains);
  }

  static String[] getOldChains(String dataOldFirstLine)
  {
    String[] chainsArray = dataOldFirstLine.split("\\s+");
    return(chainsArray);
  }



}
