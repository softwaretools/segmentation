/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Segmentation;
import java.util.*;
import java.io.*;
/**
 *
 * @author peukert
 */
public class IPA {
static String cmudict = "", ipaPath;
  static boolean cmudictRead = false;
  static boolean tagging = false;

  static String[] cmuChars = {"AA","AE","AH","AO","AW","AY","CH","DH","EH","ER","EY","HH","IH","IY","JH","NG","OW","OY","SH","TH","UH","UW","ZH","B","D","F","G","K","L","M","N","P","R","S","T","V","W","Y","Z"};
  static char[] ipaChars = {0x252, 0x259, 0x28C, 0x254, 0x263A, 0x263B, 0x2A7, 0x0F0, 0x065, 0x25C, 0x263C, 0x068, 0x26A, 0x069, 0x2A4, 0x14B, 0x2640, 0x2642, 0x283, 0x3B8, 0x28A, 0x075, 0x292, 0x062, 0x064, 0x066, 0x067, 0x06B, 0x06C, 0x06D, 0x06E, 0x070, 0x072, 0x073, 0x074, 0x076, 0x077, 0x06A, 0x07A};

  static String thisData;

//constructor
public IPA(String s){
    ipaPath = s;
}

//methods
  static boolean isLetter(char c)
  {
    return
    (
        ((c>=65)&&(c<=90))
        ||
        ((c>=97)&&(c<=122))
        ||
        (c==0xe4)
        ||
        (c==0xf6)
        ||
        (c==0xfc)
        ||
        (c==0xc4)
        ||
        (c==0xd6)
        ||
        (c==0xdc)
        ||
        (c==0xdf)
        ||
        (c==39)
    );
  }

static boolean isLetterForTags(char c)
  {
    return
    (
        ((c>=65)&&(c<=90))
        ||
        ((c>=97)&&(c<=122))
        ||
        (c==0xe4)
        ||
        (c==0xf6)
        ||
        (c==0xfc)
        ||
        (c==0xc4)
        ||
        (c==0xd6)
        ||
        (c==0xdc)
        ||
        (c==0xdf)
        ||
        (c==39)//Apostroph (bei z.B. 's - Konstruktionen)
        ||
        (c==47)//Schrägstrich (markiert Anfang eines Tags)
        ||
        (c==46) //Punkt als Satzschlusszeichen zu interpretieren
        ||
        (c==45) //minus um Konstruktionen wie "take-off" zu akzeptieren
    );
  }

  static boolean isIPALetter(char c)
  {
    boolean isIPA = false;
    for(int i=0;i<ipaChars.length;i++)
      if(c==ipaChars[i])
        isIPA = true;

    return(isIPA);
  }
  //liefert das nächste Wort zurück
  static NextWord findWord(int pos)
  {
    while((pos<thisData.length())&&!isLetter(thisData.charAt(pos)))
    {
      pos++;
    }
    String s = "";
    while((pos<thisData.length())&&isLetter(thisData.charAt(pos)))
    {
      s=s.concat(String.valueOf(thisData.charAt(pos)));
      pos++;
    }
    NextWord n = new NextWord(s,pos);
    return(n);
  }

//liefert das nächste Wort mit dem dazugehörigem Tag zurück
  static NextWord findWordTags(int pos)
  {
    while((pos<thisData.length())&&!isLetterForTags(thisData.charAt(pos)))
    {
      pos++;
    }
    String s = "";
    while((pos<thisData.length())&&isLetterForTags(thisData.charAt(pos)))
    {
      s=s.concat(String.valueOf(thisData.charAt(pos)));
      pos++;
    }
    NextWord n = new NextWord(s,pos);
    return(n);
  }

  static String convertTextToIPA(String data)
  {
    thisData = data; //Inputtext
    String dataOut = ""; //Rückgabestring
    int pos = 0; //zählt die Anzahl der Wörter im Inputtext
    while(pos<data.length()) //solange noch Wörter im Inputtext sind
    { //in pos ist Position und der String des Wortes kodiert
      
      NextWord nextWord = findWord(pos); //liefert das nächste Wort zurück
      nextWord.word = nextWord.word.toLowerCase(); //Großbuchstaben werden umgewandelt      
      String w = convertToIPA(nextWord.word); //wandelt das Wort in IPA um
      if(w!=null) //falls das Wort nicht im CMU war, ist der Rückgabestring, der nun in w steht, leer
      {
        nextWord.word = w;
        dataOut+=nextWord.word; //die position und das Wort werden angepasst
      }
      pos = nextWord.position;
    }
    return(dataOut); //IPA Text wird zurückgegeben
  }

   static String convertTextToIPAWhiteSpace(String data)
  {
    thisData = data;
    String dataOut = "";
    int pos = 0;
    while(pos<data.length())
    {
      NextWord nextWord = findWord(pos);
      nextWord.word = nextWord.word.toLowerCase();
      String w = convertToIPA(nextWord.word);
      if(w!=null)
      {
        nextWord.word = w;
        dataOut+=nextWord.word+" ";
      }
      pos = nextWord.position;
    }
    return(dataOut);
  }
//converts a tagged word into IPA; leaves tag in formal notation
   static String convertTextToIPATags(String data)
  {
    thisData = data;
    String dataOut = "";
    int pos = 0;
    while(pos<data.length())
    {
      String tag ="";
      int startTag;
      NextWord nextWord = findWordTags(pos);
      //nextWord.word = nextWord.word.toLowerCase(); //tagging operation already lowercased everything
      startTag = nextWord.word.indexOf("/"); //where in the word does the tag start
      // falls Wörter ohne "/" kommen
      if(startTag<0) {
          pos = nextWord.position;
          continue;
      }
      
      tag = nextWord.word.substring(startTag); //assigns the tag index to variable tag
      nextWord.word = nextWord.word.replace(tag, ""); //deletes tag
      String w = convertToIPA(nextWord.word); //transcribes IPA
      if(w!=null || nextWord.word.equals(".")) //if CMU entry exist or when the NextWord is a period (marker for sentence boundary)
      {
        if (nextWord.word.equals(".")) nextWord.word = ".";
        if (w!=null) nextWord.word = w;
        dataOut+=nextWord.word.concat(tag)+" ";//adds tag again and whitespace
      }
      pos = nextWord.position;
    }
    return(dataOut);
  }

  static String convertToIPA(String word)
  {
    if (!cmudictRead)
    {
      int cnt=0;
      String line = "";
      try
      { 
        FileReader fr = new FileReader(ipaPath);
        BufferedReader br = new BufferedReader(fr); 
        char [] cmubuff = new char[3511906];
        fr.read(cmubuff,0,3511906);
        fr.close();
        cmudict = String.valueOf(cmubuff);
        cmudictRead=true;
      } catch(IOException e){}
    }
    int pos = cmudict.indexOf(String.valueOf((char)0x0a).concat(word.toUpperCase().concat("  ")));
    if(pos>=0)
    {
      String cmuword = cmudict.substring(pos+3+word.length(),cmudict.indexOf(String.valueOf((char)0x0a),
                                         pos+3+word.length()));
      //deletes all numbers (prosody) in CMU-entries
      cmuword = cmuword.replaceAll("\\d","");

      for(int i=0;i<cmuChars.length;i++)
      {
        cmuword = cmuword.replace(cmuChars[i],String.valueOf(ipaChars[i]));
      }
      //deletes all whitespaces
      cmuword = cmuword.replaceAll("\\s","");
      return(cmuword);
    }
    //if not available then nothing will be returned
    return(null);
  }
  /* needed to realign ipa to text in TabbedChains*/
  static String convertIPAToASCII(String data)
  {
    for(int i=0;i<ipaChars.length;i++)
    {
      data = data.replace(String.valueOf(ipaChars[i]),cmuChars[i]);
    }
    return(data);
  }

  static String substituteDiphthongs(String data)
  {
    data=data.replace(String.valueOf((char)0x2640),String.valueOf((char)0x259)+String.valueOf((char)0x28A));
    data=data.replace(String.valueOf((char)0x263b),String.valueOf((char) 0x61)+String.valueOf((char)0x26A));
    data=data.replace(String.valueOf((char)0x263c),String.valueOf((char) 0x65)+String.valueOf((char)0x26A));
    data=data.replace(String.valueOf((char)0x2642),String.valueOf((char)0x254)+String.valueOf((char)0x26A));
    data=data.replace(String.valueOf((char)0x263a),String.valueOf((char) 0x61)+String.valueOf((char)0x28A));
    return(data);
  }

  static String resubstituteDiphthongs(String data)
  {
    data=data.replace(String.valueOf((char)0x259)+String.valueOf((char)0x28A),String.valueOf((char)0x2640));
    data=data.replace(String.valueOf((char) 0x61)+String.valueOf((char)0x26A),String.valueOf((char)0x263b));
    data=data.replace(String.valueOf((char) 0x65)+String.valueOf((char)0x26A),String.valueOf((char)0x263c));
    data=data.replace(String.valueOf((char)0x254)+String.valueOf((char)0x26A),String.valueOf((char)0x2642));
    data=data.replace(String.valueOf((char) 0x61)+String.valueOf((char)0x28A),String.valueOf((char)0x263a));
    return(data);
  }

  static String readFile(String file, boolean unicode)
  {
    String data = "";
    String line = "";
    char cData[] = new char[5*1048576];
    int i = 0;
    try
    {
      BufferedReader fr =
            new BufferedReader(
                new InputStreamReader(new FileInputStream(file),
                                unicode?"UnicodeLittle":"ISO8859_1"));
      i = fr.read(cData);
      data = new String(cData, 0, i);
      fr.close();
    } catch(IOException e){}
    return(data);
  }

  static void writeFile(String file, String data, boolean unicode)
  {
    try
    {
      BufferedWriter bw = new BufferedWriter(unicode?new OutputStreamWriter(new FileOutputStream(file),"UnicodeLittle"):new FileWriter(file));
      bw.write(data);
      bw.close();
    } catch(IOException e){}
  }

}
