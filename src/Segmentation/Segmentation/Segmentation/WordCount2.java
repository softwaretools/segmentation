/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Segmentation;
import java.util.*;
import java.io.*;
/**
 *
 * @author peukert
 */
public class WordCount2 {
    static private int runPosition = 0;
    static private String outFileResult = "";
public static void countWords(String fileFound, String lexfile, int grenzfunc, boolean ipa, String outputFilePath, int run, String outputName)
  {
   //outputName and run only needed for method writeWordsToResultFile
    //boolean ipa = args.length>3 && "ipa".equals(args[3]);

    //int x = (args.length>2)?Integer.valueOf(grenzfunc):1000000;
    int x = grenzfunc;
    outFileResult = outputName;
    runPosition = run;

    Hashtable words = new Hashtable();
    Hashtable oldWords = new Hashtable();

    String oldLexicon = lexfile;
    int runIndex = oldLexicon.lastIndexOf("_".charAt(0),oldLexicon.lastIndexOf("_".charAt(0))-1)-1;
    int runFirstIndex = oldLexicon.lastIndexOf(".".charAt(0),runIndex)+1;
    System.out.println("===>"+runFirstIndex+" "+runIndex);
    int oldRun = Integer.parseInt(oldLexicon.substring(runFirstIndex,runIndex+1))-1;
    oldLexicon = oldLexicon.substring(0,runFirstIndex)+oldRun+oldLexicon.substring(runIndex+1,oldLexicon.length());
    System.out.println("===> "+oldLexicon);
    if((new File(oldLexicon)).exists())
      oldWords = readVocabulary(oldLexicon,true);
    else
      System.out.println("previous File taken: "+oldLexicon);

    int fpos = 0;

    String f = IPA.readFile(fileFound,true)+" ";

    while(fpos<f.length())
    {
      while(fpos<f.length() && isWhiteSpace(f.charAt(fpos)))
      {
        fpos++;
        //System.out.println("-"+f.charAt(fpos));
      }
      int start=fpos;
      while(fpos<f.length() && !isWhiteSpace(f.charAt(fpos)))
      {
        fpos++;
        //System.out.println("+");
      }
      String w = f.substring(start,fpos);
      //System.out.println("#"+w);
      if(w.equals("")) break;
      if(words.containsKey(w))
      {
        words.put(w, new Integer(((Integer)words.get(w)).intValue()+1));
      }
      else
      {
        words.put(w, new Integer(1));
      }
    }

    Vector v = new Vector(words.keySet());
    for(int i=0;(i<v.size()-1) && (i<x);i++)
    {
      for(int j=i+1;j<v.size();j++)
      {
        if(((Integer)words.get((String)v.elementAt(i))).intValue() < ((Integer)words.get((String)v.elementAt(j))).intValue())
        {
          Object o = v.elementAt(i);
          v.setElementAt(v.elementAt(j),i);
          v.setElementAt(o,j);
        }
      }
    }

    for(int i=v.size()-1;i>=x;i--)
    {
      if(((Integer)words.get((String)v.elementAt(i))).intValue()<((Integer)words.get((String)v.elementAt(x-1))).intValue())
        v.removeElementAt(i);
    }

    int countNew = 0;
    for(int i=0;i<v.size();i++)
    {
      if(oldWords.containsKey((String)v.elementAt(i)))
      {
        oldWords.put((String)v.elementAt(i),((Integer)words.get((String)v.elementAt(i))).intValue()+((Integer)oldWords.get((String)v.elementAt(i))).intValue());
      }
      else
      {
        countNew++;
        oldWords.put((String)v.elementAt(i),words.get((String)v.elementAt(i)));
      }
    }
    
    //new words are written to extra file wordcount
    try
    {
      String locationWordCount = outputFilePath + File.separator + "wordcount";
      FileWriter fw = new FileWriter(locationWordCount,true);
      BufferedWriter bw = new BufferedWriter(fw);
      bw.write(lexfile+": "+countNew+" new word(s) found\n");
      bw.close();
      fw.close();
      // new words are also written to result file (last column)
      writeWordsToResultFile(countNew);
    } catch(IOException e){}

    String dataOut = "";
    v = new Vector(oldWords.keySet());
    for(int i=0;(i<v.size()-1);i++)
    {
      for(int j=i+1;j<v.size();j++)
      {
        if(((Integer)oldWords.get((String)v.elementAt(i))).intValue() < ((Integer)oldWords.get((String)v.elementAt(j))).intValue())
        {
          Object o = v.elementAt(i);
          v.setElementAt(v.elementAt(j),i);
          v.setElementAt(o,j);
        }
      }
    }

    for(int i=0;(i<v.size());i++)
    {
      dataOut+=(((String)v.elementAt(i)) + " " + ((Integer)oldWords.get((String)v.elementAt(i))).intValue())+"\n";
    }

    IPA.writeFile(lexfile,IPA.substituteDiphthongs(dataOut),true);
  }

  static boolean isWhiteSpace(char c)
  {
    return(!(IPA.isLetter(c) || IPA.isIPALetter(c)));
  }

  static Hashtable readVocabulary(String file, boolean ipa)
  {
    String s = IPA.resubstituteDiphthongs(IPA.readFile(file,true));
    Hashtable words = new Hashtable();
    String lines[] = s.split("\n");
    for(String l:lines)
    {
      if(l.matches("[a-zA-Z'"+String.copyValueOf(IPA.ipaChars)+"]+ \\d+"))
      {
        //System.out.println("!");
        int index = l.indexOf(" ");
        words.put(l.substring(0,index),Integer.valueOf(l.substring(index+1)));
      }
    }
    return(words);
  }
  //method writes the new words to the last colum in the result-files
  private static void writeWordsToResultFile(int words){
      //content of result-file read in variable
      String contentResultFile = IPA.readFile(outFileResult + "." + runPosition  + ".result", false);     
      contentResultFile = contentResultFile.replaceFirst("\t\n", "\t"+words+"\n");
      IPA.writeFile(outFileResult + "." + runPosition + ".result", contentResultFile, false);
  }
}
