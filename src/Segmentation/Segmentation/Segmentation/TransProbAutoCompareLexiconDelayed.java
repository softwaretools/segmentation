/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Segmentation;
import java.util.*;
import java.io.*;
/**
 *
 * @author peukert
 */
public class TransProbAutoCompareLexiconDelayed {

  private static int beginLen = 2;
  private static int maxlength = 2;
  private static int run = 3;
  private static int delay = 2;
  private static double limit = 0.1;
  private static ArrayList<Double> sigmaContent = new ArrayList<Double>();
  private static boolean ipa = false;
  private static boolean tagging = false;
  private static String thisData, inputFile, outputFile;

//Constructor
 public TransProbAutoCompareLexiconDelayed(String file, int maxlength, int run, int delay, int len, ArrayList<Double> sigmaContent, boolean ipa, boolean tagging, String outputFile){
     this.inputFile = file; //inputFile is only needed for comparison in Compare-method!!
     this.maxlength = maxlength;
     this.run = run;
     this.delay = delay;
     this.beginLen = len;
     this.ipa = ipa;
     this.tagging = tagging;
     this.outputFile = outputFile; //contains relevant input data!!
     this.sigmaContent = sigmaContent;
 }

 public static void presetArgs()
  {
    String dataIn = IPA.readFile(outputFile,false);
    /*note: it is important that outputFile is left here because
     the input data was copied to that file before presetArgs was called
     */
    if(ipa) IPA.writeFile(outputFile+".ipa",IPA.convertTextToIPAWhiteSpace(dataIn),ipa);
    
   
    if(ipa) dataIn = IPA.convertTextToIPA(dataIn);
    dataIn = dataIn.toLowerCase();
    
    dataIn = removeWhiteSpace(dataIn);
    int gaps[] = new int[dataIn.length()];
          
    for(int len=beginLen;len<=maxlength;len++)
    {
      System.out.println("len "+len);     
      for (int q = 0; q<sigmaContent.size(); q++)
      {        
            //sigmaContent only contains the chosen treshhold values
            limit = sigmaContent.get(q);
            
            System.out.println("limit "+limit);
            String outFile = outputFile+"."+run+"_"+len+"_"+limit;

            Vector phonemeChains1 = new Vector();
            if(run>1 && (run>delay))
            {
                String chainsfile1 = outputFile+"."+(run-1)+"_"+len+"_"+limit+".lexicon";
                String data = IPA.resubstituteDiphthongs(IPA.readFile(chainsfile1,true));
                System.out.println("===>"+chainsfile1+" "+data.length());
                String c[] = data.split("[^a-zA-Z"+String.copyValueOf(IPA.ipaChars)+"]+");
                for(int i=0;i<c.length;i++)
                {
                    for(int j=i+1;(i<c.length-1)&&(j<c.length);j++)
                    {
                        if(c[i].length()<c[j].length())
                        {
                            String tmp = c[i];
                            c[i]=c[j];
                            c[j]=tmp;
                        }
                    }
                    phonemeChains1.addElement(c[i]);
                    //System.out.println("1 "+i+c[i]);
                }
             }


                String dataOut = berechneTransProb(dataIn,len,limit,phonemeChains1);
                System.out.println("#"+dataOut);
                //System.out.println("##"+dataOut.charAt(dataOut.length()-1));

                IPA.writeFile(outFile,dataOut, true);
                if(ipa) IPA.writeFile(outFile + ".substitute",IPA.substituteDiphthongs(dataOut),true);
                if(!ipa) IPA.writeFile(outFile + ".substitute", dataOut, true);
                Vector compareResult = Compare(inputFile+"."+run,outFile,ipa);

                //if-condition meet if last Element in ArrayList was processed
                if( limit == sigmaContent.get(0))
                {
                    try
                    {
                        FileWriter fw = new FileWriter(outputFile+"."+run+".result",len!=1);//len!=1
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write("phoneme chain: "+len+"\n");
                        bw.close();
                        fw.close();
                    } catch(IOException e){}
                }
                try
                {
                    FileWriter fw = new FileWriter(outputFile+"."+run+".result",true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    bw.write(""+limit+"\t");
                    for(int r=2;r>=0;r--)
                        bw.write(""+((Integer)compareResult.elementAt(r)).intValue()+"\t");
                    for(int r=0;r<=2;r++)
                        bw.write("" + calculatePerformance(compareResult,r)+"\t");
                    bw.write("\n");
                    bw.close();
                    fw.close();
                } catch(IOException e){}
         }
      }
  }

  static String removeWhiteSpace(String data)
  {
    return(data.replaceAll("[^a-zA-ZäöüÄÖÜß"+String.valueOf(IPA.ipaChars)+"]",""));
  }

  static String berechneTransProb(String data, int len, double limit, Vector phonemeChains1)
  {
    boolean marks[] = new boolean[data.length()];
    for(int i=0;i<=data.length()-2*len;i++)
    {
      String sDouble = data.substring(i,i+2*len);
      int index = 0;
      int x = 0;
      while((index = data.indexOf(sDouble,index))>=0)
      {
        x++;
        index++;
      }

      String sSingle = data.substring(i,i+len);
      index = 0;
      int y = 0;
      while((index = data.indexOf(sSingle,index))>=0)
      {
        y++;
        index++;
      }
      if(x/(double)y<limit)
      {
        marks[i+len-1]=true;
      }
    }

    for(int j=0;j<data.length()-1;j++) //remove marks that are immediately followed by a mark
    {
      if(marks[j] && marks[j+1])
        marks[j]=false;
    }

    String dataOut="";
    for(int i=0;i<data.length();i++)
    {
      System.out.println(i);
      int chains1 = -1;
      for(int j=0;(j<phonemeChains1.size()) && (chains1<0);j++)
      {
        
        String chain = (String)phonemeChains1.elementAt(j);
        if(data.regionMatches(i,chain,0,chain.length()))
        {
          chains1=j;
        }
      }
      if(chains1>=0)

      {
        
        String chain = (String)phonemeChains1.elementAt(chains1);
        System.out.println("1 "+ chain);
        if((dataOut.length()>0) && (dataOut.charAt(dataOut.length()-1)!=32)) dataOut=dataOut.concat(" ");
        dataOut=dataOut.concat(chain+" ");
        i+=chain.length()-1;
      }
      else
      {
        dataOut=dataOut.concat(data.substring(i,i+1));
        if(marks[i])
        {
         dataOut=dataOut.concat(" ");
        }
      }
    }
    return(dataOut.trim());
  }


  static String f[] = new String[2];
  static int fpos[] = new int[2];
  static int fmiss[] = new int[2];

  static Vector Compare(String inFile, String outFile, boolean ipa)
  {

    f[0] = IPA.readFile(inFile,false);
    f[1] = IPA.readFile(outFile,true);
    fpos[0]=-1;
    fpos[1]=-1;
    fmiss[0]=0;
    fmiss[1]=0;
    if(ipa) f[0] = IPA.convertTextToIPAWhiteSpace(f[0]);
    int hit=0;
    while(((fpos[0] = nextLetter(0))>=0) && (fpos[1] = nextLetter(1))>=0)
    {
      //System.out.println(""+fpos[0]+" "+fpos[1]+" "+f[0].charAt(fpos[0])+" "+f[1].charAt(fpos[1]));
      if((fpos[0]+1<f[0].length())&&(fpos[1]+1<f[1].length()))
      {
        if(isWhiteSpace(f[0].charAt(fpos[0]+1)))
        {
          if(isWhiteSpace(f[1].charAt(fpos[1]+1)))
          {
            hit++;
          }
          else
          {
            fmiss[0]++;
          }
        }
        else
        {
          if(isWhiteSpace(f[1].charAt(fpos[1]+1)))
          {
            fmiss[1]++;
          }
        }
      }
    }

    Vector results = new Vector();
    results.addElement(new Integer(fmiss[0]));//correct segmentations
    results.addElement(new Integer(fmiss[1]));//wrong segmentations
    results.addElement(new Integer(hit));//not segmented

    return(results);
  }
  /* calculates performance of segmentation from Vector Compare data*/
  static double calculatePerformance(Vector f1Data, int index){
      /*Variable declaration*/
      double f1 = 0.0;
      double precision = 0.0;
      double recall = 0.0;
      double correct = 0.0;
      double wrong = 0.0;
      double notAtAll = 0.0;
      double total = 0.0;
      double[] measures = new double[3];
      


      /* get Data:  - correct segmentations (var correct)
       *            - wrong segmentations (var wrong)
       *            - not segmented (var notAtAll)
       */
      
          correct = ((Integer)f1Data.elementAt(2)).intValue();
          wrong = ((Integer)f1Data.elementAt(1)).intValue();
          notAtAll = ((Integer)f1Data.elementAt(0)).intValue();

      /* calculates precision*/
      if( (correct+wrong) == 0) precision = 0.0;
      else precision = correct/(correct+wrong);

      /* calculates recall*/
      total = correct + notAtAll;
      if( total == 0) recall = 0.0;
      else recall = correct/total;
      
      /* calculates f1-measure*/
      if( (precision + recall) == 0 ) f1 = 0.0;
      else f1 = 2.0*precision*recall/(precision + recall);

      /* return all three values - values are reversed because of the
         loop structure from where it is called
       */
      measures[0] = Math.round(precision*100.0)/100.0;
      measures[1] = Math.round(recall*100.0)/100.0;
      measures[2] = Math.round(f1*100.0)/100.0;

      return measures[index];
  }

  static boolean isWhiteSpace(char c)
  {
    return(!(IPA.isLetter(c) || IPA.isIPALetter(c)));
  }
  static int nextLetter(int i)
  {
    int pos = fpos[i]+1;
    while((pos<f[i].length())&&(!IPA.isLetter(f[i].charAt(pos)))&&(!IPA.isIPALetter(f[i].charAt(pos))))
      pos++;
    if(pos<f[i].length())
      return(pos);
    return(-1);
  }
}
