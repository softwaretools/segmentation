package Segmentation;
/*
 *  Usage:
 *    java processTags <inFile>
 *
 */


import java.util.*;
import java.util.regex.*;
import java.io.*;

public class processTags
{
  static String[] tags = {"N","V","RB","J"};

  public static void main(String[] args)
  {
    String data = readFile(args[0]);
    Hashtable strings = new Hashtable();

    String tagString = "";
    for (int i=0;i<tags.length;i++)
    {
      strings.put(tags[i],"");
      tagString += tags[i] + (i<tags.length-1?"|":"");
    }

    Pattern pattern = Pattern.compile("([a-zA-Z]+)/("+tagString+")[A-Z]");
    Matcher matcher = pattern.matcher(data);
    while(matcher.find())
    {
      strings.put(matcher.group(2),((String)strings.get(matcher.group(2)))+matcher.group(1)+"\n");
    }

    for (int i=0;i<tags.length;i++)
      writeFile(args[0]+"."+tags[i]+".txt",(String)strings.get(tags[i]));
  }

  static String readFile(String file)
  {
    String data = "";
    String line = "";
    char cData[] = new char[5*1048576];
    int i = 0;
    try
    {
      FileReader fr = new FileReader(file);
      i = fr.read(cData);
      data = new String(cData, 0, i);
      fr.close();
    } catch(IOException e){}
    return(data);
  }

  static void writeFile(String file,String data)
  {
    try
    {
      FileWriter fw = new FileWriter(file);
      BufferedWriter bw = new BufferedWriter(fw);
      bw.write(data);
      bw.close();
      fw.close();
    } catch(IOException e){}
  }

}
