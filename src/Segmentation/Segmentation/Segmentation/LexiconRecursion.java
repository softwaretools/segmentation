/**
 * inputs lexicon files
 * checks set and subset relations
 * Algorithm: if entry is subset of another set (entry) split subset from super set only if all
 * resulting subsets are also part of the lexicon
 */

/**
 * @author peukert
 *
 */
package Segmentation;
import java.util.*;

public class LexiconRecursion {

	/**
	 * Methoden müssen drei Arten von Inputfiles einlesen und verschieden manipulieren können
	 * 1. (wie gehabt) die Simulationsdateien, aus welchen das Paralexikon erstellt wird
	 * 2. die Dateien, auf die das Paralexikon zur Syntaxerkennung angewendet werden soll
	 * 3. Lexikonfiles, die eine interne Rekursion durchlaufen sollen
	 */
	/*class variables*/
	String lexPath = ""; //Typ 1
	int phi = 1000; //Länge/Größe des Korpus = Anzahl der Zeichen im Korpus
	boolean ipa = false;
	String lexicon = "";
	
	//werden später über GUI eingegeben
	/*constructors*/
	public LexiconRecursion(String lexPath, Boolean ipa, int phi){
		this.lexPath = lexPath; //contains text to be formated/manipulated
                this.phi = phi;
	}
	public LexiconRecursion(String lexicon){
		this.lexicon = lexicon;
                this.phi = phi;
	}	
	
	//method extracts speech adressed to Children by mother and father
	protected String formatInput1(){
		String lexData = readLexicon();
		//Auslesen von *MOT: und *FAT:
		//write all sentences in Array lines
		String[] lines = lexData.split("\\n");
		String[] formated = new String[lines.length];
		int x = 0;
		for (String f : lines){	
                    if(f.startsWith("*MOT") || f.startsWith("*FAT")){//can be extended by other abbreviations
			x++;
			formated[x]=f;
                    }
                    String[] subFormat = new String[x];
                    for (int i = 0; i<subFormat.length; i++){
                        subFormat[i] = formated[i];
                    }
		}
		return setSize(deleteCDSSigns(formated), phi);		
	}
	//method carries out a restructuring of the last? lexicon-file
	protected String formatInput2(){
        //all entries are subdivided into subset if subset is already present in the lexicon (see class description above)
		lexicon = findSubsets(lexicon);
		return lexicon;
	}
        //method formats text files, deletes Information in tagsets
        protected String formatInput3(){
            //lexData receives data (that was selected in the GUI)
            String lexData = readLexicon().trim();
            //information that is in tag is deleted
            lexData = lexData.replaceAll("<(.+?)>","");
            //replace other information irrelevant here
            lexData = lexData.replaceAll("\\&(.+?);","");
            //apostrophes
            lexData = lexData.replaceAll("\\s'", "'");
            //more than one white space replaced by one
            lexData = lexData.replaceAll("\\s+", " ");
            return setSize(lexData, phi);
        }
        //method formats csj-files;
        protected String formatInput4(){
                String lexData = readLexicon();
                //handling code for "normalizing" japanese corpora
                //replace GEM and LV
                lexData = lexData.replaceAll("GEM",""); //comment out if length is to be considered
                lexData = lexData.replaceAll("LV","");  //comment out if length is to be considered
                //replace X
                lexData = lexData.replaceAll("X", "");
                //replace content within brackets ()
                lexData = lexData.replaceAll("\\((.+?)\\)", "");
                lexData = lexData.replaceAll("\\)","");
                //replace other information irrelevant here
                lexData = lexData.replaceAll("\\&lt;(.+?);","");
                //replace double spaces
                lexData = lexData.replaceAll("\\s\\s"," ");
                //replace multichar by single char
                /*lexData = lexData.replaceAll("kw","1");
                lexData = lexData.replaceAll("gw","2");
                lexData = lexData.replaceAll("kj","3");
                lexData = lexData.replaceAll("gj","4");
                lexData = lexData.replaceAll("sj","5");
                lexData = lexData.replaceAll("zj","6");
                lexData = lexData.replaceAll("cj","7");
                lexData = lexData.replaceAll("nj","8");
                lexData = lexData.replaceAll("hj","9");
                lexData = lexData.replaceAll("ky","");
                lexData = lexData.replaceAll("gy","");
                lexData = lexData.replaceAll("sy","");
                lexData = lexData.replaceAll("zy","");
                lexData = lexData.replaceAll("cy","");
                lexData = lexData.replaceAll("ty","");
                lexData = lexData.replaceAll("dy","");
                lexData = lexData.replaceAll("ny","");
                lexData = lexData.replaceAll("hy","");
                lexData = lexData.replaceAll("Fy","");
                lexData = lexData.replaceAll("by","");
                lexData = lexData.replaceAll("py","");
                lexData = lexData.replaceAll("my","");
                lexData = lexData.replaceAll("ry","");*/

                return setSize(lexData, phi);
        }
	
	private String readLexicon(){
		String lexData = "";
		lexData = IPA.readFile(lexPath, ipa);
		return lexData;
	}
	
	private String[] formatLexicon(String rowInput){
		String[] formatedLex = rowInput.split("\\d{1,5}");
		return formatedLex;
	}
	
	private String deleteCDSSigns(String[] s){
		
		String formatedCDS = "";
		formatedCDS = arrayToString(s, " ");
		formatedCDS = formatedCDS.replaceAll(",","");
		formatedCDS = formatedCDS.replaceAll("\\*MOT:", "");
		formatedCDS = formatedCDS.replaceAll("\\*FAT:", "");
		formatedCDS = formatedCDS.replaceAll("[0-9]*", "");
		formatedCDS = formatedCDS.replaceAll("\\[.*\\]", "");
		formatedCDS = formatedCDS.replaceAll("\\&(.+?)\\s", " ");
                formatedCDS = formatedCDS.replaceAll("\\[(.+?)\\]","");
		formatedCDS = formatedCDS.replaceAll("www.", "");
		formatedCDS = formatedCDS.replaceAll("\\(","");
		formatedCDS = formatedCDS.replaceAll("\\)", "");
		formatedCDS = formatedCDS.replaceAll("\\+", " ");
		formatedCDS = formatedCDS.replaceAll("null", "");
		formatedCDS = formatedCDS.replaceAll("_", "");
		formatedCDS = formatedCDS.replaceAll("\\s+", " ");
                formatedCDS = formatedCDS.replaceAll("\\s\\.", ".");
                formatedCDS = formatedCDS.replaceAll("\\.+", ".");
		return formatedCDS;
	}
	
	private String setSize(String s, int size){
		//signs that mark sentence boundary
		char a = '.';
		char b = '?';
		char c = '!';
                char d = 39;
                char e = 32;
				
                // how large is the corpus without white spaces and additional characters? = nominalContent
                int additionalCharacters = s.replaceAll("[^\\s\\.\\?!',]","").length();
                int nominalContent = s.length()+1 - additionalCharacters;               
                //random extraction is only carried out if corpus without special character is larger than specified sample (phi)
                //100 stands for an estimate for sentences (so that they are not cut in the middle.
		if (size<nominalContent+100){
		//random number drawn over the countable signs minus phi minus an estimate for sentence
                    int beginExtraction = (int) (Math.random() * (nominalContent - size - 100));
                    int counter = 0;
                    int counterDeletes = 0;
                    int counterBoth = beginExtraction;
                    while (counter < size){
				if (s.charAt(counterBoth) == a 
                                        || s.charAt(counterBoth) == b 
                                        || s.charAt(counterBoth)  == c 
                                        || s.charAt(counterBoth) == d 
                                        || s.charAt(counterBoth) == e){
					counterDeletes++; 
				}else counter++;
				counterBoth++;
                    }   
		//define last sign:  100 added as a tolerance
                    int endExtraction = beginExtraction + counter + counterDeletes + 100;
                //substring should not tear words apart
                    while (s.charAt(beginExtraction) != e)
                    {
                        beginExtraction++;
                    }
                    while (s.charAt(endExtraction) != e )
                    {
                        endExtraction--;
                    }
		//extract sample
                    s = s.substring(beginExtraction, endExtraction);
                    System.out.println("der exrahierte String: " + s);
                    System.out.println("Länge des extrahierten Strings: " + s.length());
                    System.out.println("davon Sonderzeichen: " + additionalCharacters);
                }//end of first if-condition
		
		else System.out.println("Corpus size is smaller than the sample requested: " 
                        + nominalContent + " < " + phi + " (" + additionalCharacters
                        + " special characters contained)");
		return s;
	}
	
	private static String arrayToString(String[] a, String separator) {
	    String result = "";
	    if (a.length > 0) {
	        result = a[0];    // start with the first element
	        for (int i=1; i<a.length; i++) {
	            result = result + separator + a[i];
	        }
	    }
	    return result;
	}
	
	private String findSubsets(String lexicon){
		
		/*for (int i=0;i<lexicon.length-1; i++){
			for (int j=i+1; j<lexicon.length;j++){
				if (lexicon[i].indexOf(lexicon[j])>-1){
					String s = lexicon[j].substring(lexicon[i].length());
					//is s im lexicon gelistet? 
					//wenn ja, merken und Prozedur mit restlichen Teilstring wiederholen
				}
			}
		}*/
		return lexicon;
	}

}
