/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Segmentation;
import java.io.*;
import java.util.*;
//import edu.stanford.nlp.tagger.maxent.MaxentTagger;
/**
 *
 * @author peukert
 */
public class PreProcessing {
    /* in case stanford-postagger is to be used
     args[] = {-mx300m} {-classpath} {stanford-postagger.jar} {edu.stanford.nlp.tagger.maxent.MaxentTagger} {-model models/bidirectional-wsj-0-18.tagger} {-textFile} {inputName.txt} {>} {outputTagName.txt}
    */
    private String lexiconName = "";
    private String outputTagName = "";
    private String outputName = "";
    private Boolean tagging = false;
    private Boolean ipa = false;

    //constructor
    public PreProcessing(String lexiconName, String outputTagName, String outputName, Boolean ipa){
        this.lexiconName = lexiconName;
        this.outputTagName = outputTagName;
        this.outputName = outputName;
        tagging = true;
        this.ipa = ipa;
    }
    /* //führt Tagging einer ausgewählten Datei mit dem Stanford Postagger durch
    static void startTagger() throws Exception {
    //String startTagger[] = {"-mx500m", "-classpath stanford-postagger.jar", "edu.stanford.nlp.tagger.maxent.MaxentTagger", "-model", "models\\bidirectional-wsj-0-18.tagger", "-textFile sample-input.txt > sample-output.txt"};
    String startTagger[] = {"-mx500m", "-model", "models\\bidirectional-wsj-0-18.tagger", "-textFile sample-input.txt > sample-output.txt"};
    //String startAnotherProg[] = {"java", "-jar", "stanford-postagger.jar" };
    edu.stanford.nlp.tagger.maxent.MaxentTagger.main(startTagger);

        ProcessBuilder pb = new ProcessBuilder(startTagger);
        pb.directory( new File("D:/Habilitation/Software/Tagging/stanford-postagger-2008-09-28/") );
        Process p = pb.start();
        p.waitFor();

        writeProcessOutput(p);
     
    }*/

    /*
     */

    /*compares entries of the last or a specified lexicon file with .tagipa;
     * outputs syntactic pattern for each sentence
     */
    protected void matchPatterns(){

        String tagFile = "";
        String lexFile = "";
        if(ipa){
      /* tagged file is transcribed into IPA*/
            String dataIn = IPA.readFile(outputTagName, false);
            IPA.writeFile(outputName + ".tagipa", IPA.convertTextToIPATags(dataIn),ipa);
      /* ipa tagfile assigned to variable*/
            tagFile = IPA.readFile(outputName + ".tagipa", true);
            lexFile = IPA.readFile(lexiconName, false); //!!warum ist das so und nicht true!!
        }
      /* non IPA tagfile assigned to variable*/
        else{
            tagFile = IPA.readFile(outputTagName, false);
            lexFile = IPA.readFile(lexiconName, false);
        }

         //transfer Strings in lists
         System.out.println("Lexicon: "+lexFile);
         System.out.println("tag information: " + tagFile);
		String[] lexEntry = lexFile.split("\\d{1,3}");
		String[] tagEntry = tagFile.split("\\s");

		//remove white spaces before and after
		for(int x=0; x<lexEntry.length; x++) {
			lexEntry[x]=lexEntry[x].trim();
			System.out.println(lexEntry[x]);
		}
		for(int y=0; y<tagEntry.length; y++) {
			tagEntry[y]=tagEntry[y].trim();
			//System.out.println(tagEntry[y]);
		}

		//match lexicon with tags
		String[] tagPattern = new String[tagEntry.length];
                for (int x=0; x<lexEntry.length; x++){
                    for( int y=0; y<tagEntry.length; y++){
                        if (lexEntry[x].regionMatches(0,tagEntry[y],0,lexEntry[x].length())){
                            if(tagPattern[y] == null)
                            	tagPattern[y]=tagEntry[y];
                        } // end of first if
                    } // end of second for-loop tagEntry
                } //end of first for-loop lexEntry

         /*TO DO: (dependent on the tagger used)
          *insert periods, question & exclamation marks in pattern String if tagfile requires it
          *in such a case: adjust the two split methods
          *. stands for end of sentence
          */
                for (int x=0; x<tagEntry.length; x++) {
                	if (tagEntry[x].substring(tagEntry[x].indexOf("_")+1).equals(".")){ //Windows: "/"; MAC"_"
                		tagPattern[x] =  tagEntry[x];
                	}
         //mark null-slots with xxx
                	else if (tagPattern[x] == null)
                		tagPattern[x] = "xxx";
                }

         //output word with tags
                for(int x=0; x<tagPattern.length; x++) {
                        tagPattern[x]=tagPattern[x].trim();
                        System.out.print(tagPattern[x] + " ");
                }
                System.out.println();

         //remove words from tag pattern
                for(int x=0; x<tagPattern.length; x++) {
                	if (!tagPattern[x].equals("xxx")){
                		tagPattern[x] = tagPattern[x].substring(tagPattern[x].indexOf("_")+1); //Windows: "/"; MAC"_"
                	}
                System.out.print(tagPattern[x] + " ");
                }
                System.out.println();

         //output pattern without missed words
                System.out.println("*************Pattern without placeholders****************");
                for(int x=0; x<tagPattern.length; x++){
                	if (!tagPattern[x].equals("xxx")) System.out.print(tagPattern[x] + " ");
                }
                System.out.println();
         //output tag pattern on sentence level
                String sentence= arrayToString(tagPattern, " ");
                String[] sentencePattern = sentence.split("\\.");
                for (int x=0; x<sentencePattern.length; x++) {
                	sentencePattern[x] = sentencePattern[x].trim();
                	int y = x + 1;
                	System.out.println("Sentence Pattern " + y + ": "  + sentencePattern[x]);
                }

         //output words including placeholders
                freqDist(sentencePattern);
                System.out.println();

         // delete placeholders xxx
                for(int z=0; z<tagPattern.length; z++){
                	if(tagPattern[z].equals("xxx")){
                		tagPattern[z] = tagPattern[z].replace(tagPattern[z], "");
                	}
                }
                String noPlaceholderSentence = arrayToString(tagPattern, " ");
        //restrict whitespace to one
                noPlaceholderSentence = noPlaceholderSentence.replaceAll(" +", " ");
                String[] noPlaceholderPattern = noPlaceholderSentence.split("\\.");
                for(int x=0; x<noPlaceholderPattern.length; x++){
                	//delete whitespaces before and after respective unit
                	noPlaceholderPattern[x] = noPlaceholderPattern[x].trim();

                }

          //output most frequent pattern without words missed
                freqDist(noPlaceholderPattern);
                System.out.println();
    } //end of method

    static void writeProcessOutput(Process process) throws Exception{
            InputStreamReader tempReader = new InputStreamReader(
                new BufferedInputStream(process.getInputStream()));
            BufferedReader reader = new BufferedReader(tempReader);
            //Here it should give feedback for the tagger systems output
            while (true){
                String line = reader.readLine();
                if (line == null){
                    break;
                }
                System.out.println(line);
            }
    }
    //method sorts and counts occurrences
    private static void freqDist(String[] sentencePattern){
		if (sentencePattern.length>1){//checks if at least two entries can be compared
			//sort Array
			Arrays.sort(sentencePattern);
			//counting procedure start
			for(int x=0; x<sentencePattern.length; x++){
				System.out.println(sentencePattern[x]);
			}
            for(int x=0; x<sentencePattern.length; x++){
            	int i = 1;
                for(int y=x+1; y<sentencePattern.length; y++){
                	if (sentencePattern[x].equals(sentencePattern[y]) && !sentencePattern[x].equals("")) {
                		// to avoid double counts, counted elements are deleted
                		sentencePattern[y] = sentencePattern[y].replace(sentencePattern[y], "");
                		i++; // number of occurrences
                	}
                }
            	if (i>1) System.out.println("Ergebnis: " + sentencePattern[x] + " : " + i );
            } //end of for-loop
        } //end of first if
        else System.out.println("There is only one entry in the File!");
	}

	// method to transverse a Stringarray to a String
	private static String arrayToString(String[] a, String separator) {
	    String result = "";
	    if (a.length > 0) {
	        result = a[0];    // start with the first element
	        for (int i=1; i<a.length; i++) {
	            result = result + separator + a[i];
	        }
	    }
	    return result;
	}
}

